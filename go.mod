module bitbucket.org/graph-ql-schema/nullable

go 1.14

require (
	github.com/graphql-go/graphql v0.7.9
	github.com/valyala/fasthttp v1.14.0
)
