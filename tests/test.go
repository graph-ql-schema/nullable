package main

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"bytes"
	"encoding/json"
	"github.com/graphql-go/graphql"
	"github.com/valyala/fasthttp"
	"log"
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"list": &graphql.Field{
				Type:        graphql.NewList(productType),
				Description: "Get product list",
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					return nil, nil
				},
			},
		},
	})

var productType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Product",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: nullable.NewNullable(graphql.Int),
			},
		},
	},
)

var mutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Mutation",
	Fields: graphql.Fields{
		"delete": &graphql.Field{
			Type:        productType,
			Description: "Delete product by id",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: nullable.NewNullable(graphql.Int),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				arg := params.Args[`id`]
				log.Println(arg)

				return nil, nil
			},
		},
	},
})

var schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    queryType,
		Mutation: mutationType,
	},
)

// Данные запроса GraphQL
type graphQlData struct {
	Query         string                 `json:"query"`
	OperationName string                 `json:"operationName"`
	Variables     map[string]interface{} `json:"variables"`
}

func main() {
	fasthttp.ListenAndServe(
		`0.0.0.0:8080`,
		func(ctx *fasthttp.RequestCtx) {
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
			ctx.Response.Header.Set("Content-Type", "application/json")

			if string(ctx.Method()) == "OPTIONS" {
				ctx.SetStatusCode(fasthttp.StatusOK)
				return
			}

			var data graphQlData
			if err := json.NewDecoder(bytes.NewReader(ctx.Request.Body())).Decode(&data); err != nil {
				ctx.SetStatusCode(400)

				_ = json.NewEncoder(ctx).Encode(map[string]string{
					"error": "Failed to parse request. Request should be in JSON format.",
				})

				return
			}

			var params = graphql.Params{
				Schema:         schema,
				RequestString:  data.Query,
				VariableValues: data.Variables,
				OperationName:  data.OperationName,
			}

			statusCode := 200
			result := graphql.Do(params)
			if len(result.Errors) > 0 {
				statusCode = 400
			}

			ctx.SetStatusCode(statusCode)
			_ = json.NewEncoder(ctx).Encode(result)
		},
	)
}
