package nullable

import (
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
	"strings"
)

// Хранилище nullable singleton объектов
type nullObject struct {
	baseType *graphql.Scalar
	object   graphql.Type
}

var nullableObjects = map[string]*nullObject{}

// Базовые скалярные типы не умеют обрабатывать Null значения для скаляров, даже если они объявлены
// без !. По сути для аргумента значение вида Int! и Int является однотипным, что не позволит передать
// null в качестве значения на сторону сервера. Данный тип решает эту задачу и позволяет вам передавать
// в качестве значения для него Null, Nil или Undefined, что по сути будет являться установкой Nil значения
func NewNullable(graphQlType *graphql.Scalar) graphql.Type {
	if obj, ok := nullableObjects[graphQlType.Name()]; ok {
		return obj.object
	}

	name := fmt.Sprintf(`Nullable%v`, graphQlType.Name())
	obj := *graphql.NewScalar(graphql.ScalarConfig{
		Name: name,
		Description: fmt.Sprintf(
			"Скалярный тип `%v` представляет собой комбинацию значений `NULL` и значений типа `%v`. Значения основного типа можно передавать в том формате, в котором их принимает этот тип. Например для числа можно передавать значение без кавычек. Для передачи `NULL` значений необходимо передавать их в строковом формате, а именно `\"null\"`, `\"nil\"`, `\"undefined\"`. Данное ограничение обусловлено наличием в системе запрета на передачу не зарезервированных типами ENUM значений.",
			name,
			graphQlType.Name(),
		),
		Serialize: func(value interface{}) interface{} {
			if val, ok := value.(string); ok {
				if val == "null" || val == "nil" || val == "undefined" {
					return nil
				}
			}

			return graphQlType.Serialize(value)
		},
		ParseValue: func(value interface{}) interface{} {
			if val, ok := value.(string); ok {
				if val == "null" || val == "nil" || val == "undefined" {
					return nil
				}
			}

			return graphQlType.ParseValue(value)
		},
		ParseLiteral: func(valueAST ast.Value) interface{} {
			switch val := valueAST.(type) {
			case *ast.StringValue:
				str := strings.ToLower(val.Value)
				if str == "null" || str == "nil" || str == "undefined" {
					return "nil"
				}

				return graphQlType.ParseLiteral(valueAST)
			default:
				return graphQlType.ParseLiteral(valueAST)
			}
		},
	})

	nullObj := nullObject{
		baseType: graphQlType,
		object:   &obj,
	}

	nullableObjects[graphQlType.Name()] = &nullObj
	return nullableObjects[graphQlType.Name()].object
}

// Проверяет, является ли значение Nullable
func IsNullable(scalar *graphql.Scalar) bool {
	for _, nullable := range nullableObjects {
		if nullable.object.Name() == scalar.Name() {
			return true
		}
	}

	return false
}

// Получение базового типа nullable scalar
func GetBaseType(scalar *graphql.Scalar) *graphql.Scalar {
	for _, nullable := range nullableObjects {
		if nullable.object.Name() == scalar.Name() {
			return nullable.baseType
		}
	}

	return nil
}
